package com.restPokemon.Pokemon;


import com.restPokemon.Pokemon.DAO.PokemonRepository;
import com.restPokemon.Pokemon.Entity.Pokemon;
import com.restPokemon.Pokemon.Service.ServiceDTO;
import com.restPokemon.Pokemon.Transactional.ServiceTransactional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ServiceTest {

    @MockBean
    PokemonRepository pokemonRepository;
    @MockBean
    Pokemon pokemon;

    @MockBean
    ServiceTransactional serviceTransactional;
    @Test
    public void ServiceTest(){

       // boolean isSaveAll = serviceTransactional.generateViaSpring(any());

        //Mockito.verify(serviceTransactional).generateViaSpring(any());
        Mockito.verify(pokemonRepository).getOne(any());
        ServiceDTO serviceDTO = new ServiceDTO();

        Assert.assertNotNull(serviceDTO);


    }

}
