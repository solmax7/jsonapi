<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>

<head>

     <meta charset="UTF-8">
     <title>Покемоны</title>
     <link rel="stylesheet" href="css/index.css" media="screen" type="text/css" />
</head>

<body>

      <h1>Покемоны</h1>
      <div><a href="/pokemons/${next}">Следующая страница</a>
      </div>
          <p></p>
          <div><a href="/pokemons/${previous}">Предыдущая страница</a>
      </div>
     <table>

     <tr>

                                          <td><p> "Имя"</p></td>
                                          <td><p> "URL"</p></td>

                            </tr>

     		<c:forEach var="pokemon" items="${pokemonList}" varStatus="сounter">
     			<tr>

     				<td><a href="/pokemon/${pokemon.name}">${pokemon.name}</a></td>
     				<td><a href="/pokemon/${pokemon.id}">${pokemon.url}</a></td>

     			</tr>
     		</c:forEach>
     	</table>

     	<div>

     	${tour.description}


     	</div>



</div>
	<div class="footer">Покемоны<span class="separator">|</span>&nbsp; <a href="http://api.ru">Pokemons</a> </div>
	</div>
</body>
</html>