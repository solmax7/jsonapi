<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>

<head>

    <meta charset="UTF-8">
    <title>Ability</title>
    <link rel="stylesheet" href="css/index.css" media="screen" type="text/css" />
</head>

<body>

<h1>Ability</h1>
<table>

    <tr>
        <td><p> "Id"</p></td>
        <td><p> "Имя"</p></td>
        <td><p> "URL"</p></td>

    </tr>

    <c:forEach var="ability" items="${abilityList}" varStatus="сounter">
        <tr>

            <td><a href="/pokemon/${ability.name}">${ability.name}</a></td>
            <td><a href="/pokemon/${ability.url}">${ability.url}</a></td>

        </tr>
    </c:forEach>
</table>

<div>

    ${tour.description}


</div>



</div>
<div class="footer">Покемоны<span class="separator">|</span>&nbsp; <a href="http://api.ru">Pokemons</a> </div>
</div>
</body>
</html>