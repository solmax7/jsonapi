<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>

<head>

    <meta charset="UTF-8">
    <title>${pokemon.name}</title>
    <link rel="stylesheet" href="css/index.css" media="screen" type="text/css" />
</head>

<body>

<h1>Покемон</h1>

<table>

    <tr>

        <td><p> "Свойство"</p></td>
        <td><p> "Скрытый"</p></td>
        <td><p> "Слот"</p></td>

    </tr>

    <c:forEach var="ability" items="${ability}" varStatus="сounter">
        <tr>

            <td><a href="/pokemon/${pokemon.id}">${ability.ability.name}</a></td>
            <td><a href="/pokemon/${pokemon.id}">${ability.is_hidden}</a></td>
            <td><a href="/pokemon/${pokemon.id}">${ability.slot}</a></td>


        </tr>
    </c:forEach>
</table>

<div>

</div>

</body>
</html>