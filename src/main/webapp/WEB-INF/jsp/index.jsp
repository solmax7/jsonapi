<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Покемоны</title>
</head>
<body>
<h1>Главная страница</h1>
<p>Страница покемонов</p>
<div><a href="/pokemons">Покемоны</a></div>
<p>Страница свойств покемонов</p>
<div><a href="/abilities">Свойства покемонов</a></div>

</body>
</html>
