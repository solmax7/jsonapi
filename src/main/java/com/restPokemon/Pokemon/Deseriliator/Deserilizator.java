package com.restPokemon.Pokemon.Deseriliator;

import com.restPokemon.Pokemon.Controller.PokemonController;
import com.restPokemon.Pokemon.Entity.AbilityPull;
import com.restPokemon.Pokemon.Entity.Pokemon;
import com.restPokemon.Pokemon.Entity.PokemonDescription;
import com.restPokemon.Pokemon.Entity.PokemonList;
import org.apache.log4j.Logger;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
public class Deserilizator {

    final static Logger logger = Logger.getLogger(PokemonController.class);

    public List PokemonDeserilizator(String url) {
//
        try {

            HttpHeaders headers = new HttpHeaders();

            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("User-Agent", "Google Chrome");
            HttpEntity<String> entity = new HttpEntity<String>(headers);

            RestTemplate restTemplate = new RestTemplate();

            ResponseEntity<PokemonDescription> response = restTemplate.exchange(url, //

                    HttpMethod.GET, entity, PokemonDescription.class);

            List result = Arrays.asList(response.getBody());

            System.out.println(result);
            logger.info(result);
            return result;

        } catch (Exception e) {

            logger.trace(e.getMessage(), e);

        }

        return null;
    }

    public List Deserilizator(String url) {



        if (url == "") {

            url = "https://pokeapi.co/api/v2/pokemon/";

        }

        try {
            // HttpHeaders
            HttpHeaders headers = new HttpHeaders();

            //headers.setAccept(Arrays.asList(new MediaType[]{MediaType.APPLICATION_JSON}));
            // Request to return JSON format
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("User-Agent", "Google Chrome");
            HttpEntity<String> entity = new HttpEntity<String>(headers);

            // RestTemplate
            RestTemplate restTemplate = new RestTemplate();
//
//            // Send request with GET method, and Headers.
//            // ResponseEntityt<Pokemonlist> response = restTemplate.exchange("http://pokeapi.co/api/v2/pokemon"
            ResponseEntity<PokemonList> response = restTemplate.exchange(url, //
                    //HttpMethod.GET, entity, new ParameterizedTypeReference<<Pokemonlist>(){});
                    HttpMethod.GET, entity, PokemonList.class);


            List result = Arrays.asList(response.getBody());

            List<Pokemon> Arraypokemons = ((PokemonList) result.get(0)).getResults();

            String pattern = "/[0-9]/|/[0-9][0-9]/|/[0-9][0-9][0-9]/";
            Pattern r = Pattern.compile(pattern);

            for (Pokemon pokemon: Arraypokemons)
                  {

                       String id = pokemon.getUrl().substring(pokemon.getUrl().length()-2, pokemon.getUrl().length());

                      Matcher m = r.matcher(pokemon.getUrl());
                      while(m.find())
                          //System.out.println(m.group());
                      pokemon.setId(Long.parseLong(m.group().replaceAll("/","")));
                  }

            System.out.println(result);
            logger.info(result);
            return result;

        } catch (Exception e) {

            logger.trace(e.getMessage(), e);

        }
        // HttpEntity<String>: To get result as String.
        return null;
    }

    public List DeserilizatorAbility() {

        try {
            // HttpHeaders
            HttpHeaders headers = new HttpHeaders();

            //headers.setAccept(Arrays.asList(new MediaType[]{MediaType.APPLICATION_JSON}));
            // Request to return JSON format
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("User-Agent", "Google Chrome");
            HttpEntity<String> entity = new HttpEntity<String>(headers);

            // RestTemplate
            RestTemplate restTemplate = new RestTemplate();
//
//            // Send request with GET method, and Headers.
//            // ResponseEntityt<Pokemonlist> response = restTemplate.exchange("http://pokeapi.co/api/v2/pokemon"
            ResponseEntity<AbilityPull> response = restTemplate.exchange("https://pokeapi.co/api/v2/ability/?offset=0&limit=293", //
                    //HttpMethod.GET, entity, new ParameterizedTypeReference<<AbilityList>(){});
                    HttpMethod.GET, entity, AbilityPull.class);


            List result = Arrays.asList(response.getBody());

            System.out.println(result);
            logger.info(result);
            return result;

        } catch (Exception e) {

            logger.trace(e.getMessage(), e);

        }
        // HttpEntity<String>: To get result as String.
        return null;
    }


    public List AllDeserilizator() {

       String url = "https://pokeapi.co/api/v2/pokemon?offset=0&limit=964";

        try {
            // HttpHeaders
            HttpHeaders headers = new HttpHeaders();

            //headers.setAccept(Arrays.asList(new MediaType[]{MediaType.APPLICATION_JSON}));
            // Request to return JSON format
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("User-Agent", "Google Chrome");
            HttpEntity<String> entity = new HttpEntity<String>(headers);

            // RestTemplate
            RestTemplate restTemplate = new RestTemplate();
//
//            // Send request with GET method, and Headers.
//            // ResponseEntityt<Pokemonlist> response = restTemplate.exchange("http://pokeapi.co/api/v2/pokemon"
            ResponseEntity<PokemonList> response = restTemplate.exchange(url, //
                    //HttpMethod.GET, entity, new ParameterizedTypeReference<<Pokemonlist>(){});
                    HttpMethod.GET, entity, PokemonList.class);


            List result = Arrays.asList(response.getBody());

            List<Pokemon> Arraypokemons = ((PokemonList) result.get(0)).getResults();

            String pattern = "/[0-9]/|/[0-9][0-9]/|/[0-9][0-9][0-9]/";
            Pattern r = Pattern.compile(pattern);

            for (Pokemon pokemon: Arraypokemons)
            {

                String id = pokemon.getUrl().substring(pokemon.getUrl().length()-2, pokemon.getUrl().length());

                Matcher m = r.matcher(pokemon.getUrl());
                while(m.find())
                    //System.out.println(m.group());
                    pokemon.setId(Long.parseLong(m.group().replaceAll("/","")));
            }

            System.out.println(result);
            logger.info(result);
            return result;

        } catch (Exception e) {

            logger.trace(e.getMessage(), e);

        }
        // HttpEntity<String>: To get result as String.
        return null;
    }


}




