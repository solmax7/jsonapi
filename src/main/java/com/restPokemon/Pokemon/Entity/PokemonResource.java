package com.restPokemon.Pokemon.Entity;

import lombok.Data;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Component;

@Data
@Component
public class PokemonResource extends ResourceSupport {

    private String name;
    private String url;
    //private Long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
