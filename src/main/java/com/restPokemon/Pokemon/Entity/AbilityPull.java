package com.restPokemon.Pokemon.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.stereotype.Component;
import java.util.List;


@Component
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class AbilityPull {

    private List<Ability> results;

}
