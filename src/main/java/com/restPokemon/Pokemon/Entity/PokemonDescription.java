package com.restPokemon.Pokemon.Entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonDescription {

    //private Pokemon pokemon;
    private String name;
    private List<AbilityList> abilities;
    private int base_experience;

}
