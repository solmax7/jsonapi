package com.restPokemon.Pokemon.Entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@JsonAutoDetect(creatorVisibility = JsonAutoDetect.Visibility.PROTECTED_AND_PUBLIC)
@JsonIgnoreProperties(ignoreUnknown = true)
//@Table(name = "ABILITY")
public class Ability {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "Name", length = 64)
    private String name;
    @Column(name = "Url", length = 64)
    private String url;
//    @ManyToMany(mappedBy = "pokemn_id", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    private List<Pokemon> pokemonList;


}
