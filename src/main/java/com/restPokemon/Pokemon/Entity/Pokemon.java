package com.restPokemon.Pokemon.Entity;


import lombok.Data;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
//@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonAutoDetect(creatorVisibility = JsonAutoDetect.Visibility.PROTECTED_AND_PUBLIC)
//@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Table(name = "POKEMON")
public class Pokemon {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "Name", length = 64)
    private String name;
    @Column(name = "Url", length = 64)
    private String url;
    @Column(name = "Ability", length = 64)
    @OneToMany(mappedBy = "id", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<Ability> abilityList;

}
