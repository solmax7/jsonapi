package com.restPokemon.Pokemon.Entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Component
//@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonAutoDetect(creatorVisibility = JsonAutoDetect.Visibility.PROTECTED_AND_PUBLIC)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PokemonList {


    private Long id;

    private String next;

    private String previous;

    private List<Pokemon> results;

    public List<Pokemon> getResults() {
        return results;
    }
}
