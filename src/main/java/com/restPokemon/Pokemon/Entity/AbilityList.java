package com.restPokemon.Pokemon.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.ArrayList;


@Data
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class AbilityList {


    private Ability ability;

    private Boolean is_hidden;
    private int slot;

}
