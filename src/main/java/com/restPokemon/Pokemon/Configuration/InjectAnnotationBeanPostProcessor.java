package com.restPokemon.Pokemon.Configuration;

import com.restPokemon.Pokemon.Controller.PokemonController;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;


@Component
public class InjectAnnotationBeanPostProcessor implements BeanPostProcessor {

    final static Logger log = Logger.getLogger(InjectAnnotationBeanPostProcessor.class);

        @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        log.info(beanName);
        return bean;
    }

}
