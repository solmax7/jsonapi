//package com.restPokemon.Pokemon.Configuration;
//
//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.beans.factory.config.BeanDefinition;
//import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
//import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.Optional;
//
//
//@Configuration
//public class MockBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
//
//    @Value("$(mock.enable)")
//    private boolean useMock;
//
//
//    @Override
//    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
//
//        if (!useMock) {
//
//            return;
//        }
//
//        final String[] beanDefinitionNames = beanFactory.getBeanDefinitionNames();
//        for (String beanName : beanDefinitionNames) {
//
//            final BeanDefinition beanDefinition = beanFactory.getBeanDefinition(beanName);
//            Class<?> beanClass = null;
//            try {
//                beanClass = Class.forName(beanDefinition.getBeanClassName());
//            } catch (Exception ignored) {
//            }
//
//            Optional.ofNullable(beanClass).map(clazz -> clazz.getAnnotation(UseMock.class))
//                    .map(UseMock::mockClass)
//                    .ifPresent(foundClass -> beanDefinition.setBeanClassName(foundClass.getName()));
//        }
//    };
//}
//
//
//
