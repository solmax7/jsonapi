package com.restPokemon.Pokemon.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.restPokemon.Pokemon.Deseriliator.Deserilizator;
import com.restPokemon.Pokemon.Entity.Pokemon;
import org.apache.naming.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Configuration
@Service
public class DomainController {

    @Value("${use.flag}")
    private boolean flag;
    @Autowired
    private Deserilizator deserilizator;


//    @Bean
//    public  PokemonServise() {
//
//        if (flag) {
//
//            String pokemon = deserilizator.PokemonDeserilizator("");
//            return pokemon;
//            }
//
//        else {
//
//           return "Сервис находится в стадии разработки";
//
//        }
//
//    }

    @Bean
    public static BeanFactoryPostProcessor beanFactoryPostProcessor(Environment environment){

        return beanFactory -> {

            BindResult<Boolean> result = Binder.get(environment).bind("mock.enable", Boolean.class);
            if (result.get().equals(Boolean.FALSE)) {

                return;
            }
            final String[] beanDefinitionNames = beanFactory.getBeanDefinitionNames();
            for (String beanName : beanDefinitionNames) {

                final BeanDefinition beanDefinition = beanFactory.getBeanDefinition(beanName);
                Class<?> beanClass = null;
                try {
                    beanClass = Class.forName(beanDefinition.getBeanClassName());
                } catch (Exception ignored) {
                }

                Optional.ofNullable(beanClass).map(clazz -> clazz.getAnnotation(UseMock.class))
                        .map(UseMock::mockClass).ifPresent(foundClass -> beanDefinition.setBeanClassName(foundClass.getName()));
            }
        };
    }
}
