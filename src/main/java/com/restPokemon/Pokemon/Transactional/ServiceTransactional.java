package com.restPokemon.Pokemon.Transactional;

import com.restPokemon.Pokemon.DAO.AbilityRepository;
import com.restPokemon.Pokemon.DAO.PokemonRepository;
import com.restPokemon.Pokemon.Deseriliator.Deserilizator;
import com.restPokemon.Pokemon.Entity.Ability;
import com.restPokemon.Pokemon.Entity.Pokemon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ServiceTransactional {

    @Autowired
    PokemonRepository pokemonRepository;
    @Autowired
    AbilityRepository abilityRepository;

    @Autowired
    Deserilizator deserilizator;

    public boolean generateViaSpring(List<Pokemon>pokemons){

        return pokemonRepository.saveAll(pokemons) != null;

    }

    public boolean SaveAllAbility(List<Ability>abilities){

        return abilityRepository.saveAll(abilities) != null;

    }

    @Transactional
    public boolean generateJPA(List<Pokemon>pokemons){

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("PERSISTENCE_UNIT_NAME");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        boolean success;
        try {
            transaction.begin();
            success = pokemonRepository.saveAll(pokemons) != null;
            transaction.commit();
        }
        catch (Exception ex){
            transaction.rollback();
            throw ex;
        }
        return success;
    }
}
