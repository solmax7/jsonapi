package com.restPokemon.Pokemon.DAO;


import com.restPokemon.Pokemon.Entity.Pokemon;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class MyCustomRepository {

    @PersistenceContext
    EntityManager entityManager;

    public List<Pokemon> getByName(String name){


        Query query = InitQuery(name);

        return query.getResultList();

    }

    private Query InitQuery(String name){

        Query query = entityManager.createNativeQuery("SELECT p.url FROM pokemon as p");
        //query.setParameter(1, name);
        return query;

    }

}
