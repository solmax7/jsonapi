package com.restPokemon.Pokemon.DAO;

import com.restPokemon.Pokemon.Entity.Pokemon;
import javafx.application.Application;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MyRepository extends JpaRepository<Pokemon, Long> {

//@Query("select pokemon.name from Pokemon pokemon where pokemon.id = :#{filter}");
//
//Page<Pokemon> searchApplications(Pageable pageable, @Param("filter")ApplicationFilter filter)

    @Query("select pokemon from Pokemon pokemon")
    Page<Pokemon> findAllById(Pageable pageable, int id);

}
