package com.restPokemon.Pokemon.DAO;

import com.restPokemon.Pokemon.Entity.Pokemon;
import com.restPokemon.Pokemon.Entity.PokemonList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface PokemonRepository extends JpaRepository<Pokemon, Long> {


}
