package com.restPokemon.Pokemon.DAO;

import com.restPokemon.Pokemon.Entity.Ability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AbilityRepository extends JpaRepository<Ability, Long> {
}
