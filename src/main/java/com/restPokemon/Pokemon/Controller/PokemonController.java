package com.restPokemon.Pokemon.Controller;


import com.restPokemon.Pokemon.Configuration.DomainController;
import com.restPokemon.Pokemon.DAO.AbilityRepository;
import com.restPokemon.Pokemon.DAO.PokemonRepository;
import com.restPokemon.Pokemon.DAO.SaveAbilities;
import com.restPokemon.Pokemon.Deseriliator.Deserilizator;
import com.restPokemon.Pokemon.Entity.*;
import com.restPokemon.Pokemon.HateOAS.ServiceResourceAssembler;
import com.restPokemon.Pokemon.Service.ServiceDTO;
import com.restPokemon.Pokemon.Transactional.ServiceTransactional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@RestController
//@RequestMapping("pokemon")
public class PokemonController {


    final static Logger logger = Logger.getLogger(PokemonController.class);
    @Autowired
    private Deserilizator deserilizator;
    @Autowired
    private DomainController domainController;
    @Autowired
    private ServiceResourceAssembler serviceResourceAssembler;
    @Autowired
    private PokemonRepository pokemonRepository;
    @Autowired
    private AbilityRepository abilityRepository;
    @Autowired
    private ServiceDTO serviceDTO;
    @Autowired
    private ServiceTransactional serviceTransactional;
    @Autowired
    private SaveAbilities saveAbilities;


    @GetMapping("/")
    //public List<Map<String, String>> list() {
    public ModelAndView index() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        return modelAndView;

    }

    @GetMapping("/getall")
    @Transactional(propagation = Propagation.REQUIRED)
    public List GetAll() {

        List pokemons = (List<Pokemon>) deserilizator.AllDeserilizator();

        serviceTransactional.generateViaSpring(((PokemonList) pokemons.get(0)).getResults());
        //serviceTransactional.generateJPA(((PokemonList) pokemons.get(0)).getResults());
        return pokemons;
    }

    @GetMapping("/getallability")
    @Transactional(propagation = Propagation.REQUIRED)
    public List GetAllAbility() {

        List abilities = (List<Ability>) deserilizator.DeserilizatorAbility();

        serviceTransactional.SaveAllAbility(((AbilityPull) abilities.get(0)).getResults());
        //serviceTransactional.generateJPA(((PokemonList) pokemons.get(0)).getResults());
        return abilities;
    }

    @GetMapping("/pokemon/{id}")

    public PokemonResource PokemonPage(@PathVariable int id) {

       // PokemonDescription pokemonDescription = pokemonDescriptionRepository.getOne(pokemonRepository.getOne((long) id).getId());

       // if (pokemonDescription.equals(null)) {

            String url = "https://pokeapi.co/api/v2/pokemon/" + id + "/";

            // url = "https://pokeapi.co/api/v2/pokemon/1/";
//        Optional<Pokemon> pokemonDescription = pokemonRepository.findById((long) id);
//        if ()

        PokemonDescription  pokemonDescription = (PokemonDescription) deserilizator.PokemonDeserilizator(url).get(0);

       // }

        ModelAndView modelAndView = new ModelAndView();

        saveAbilities.saveById(pokemonDescription.getAbilities(), pokemonDescription.getName());

        modelAndView.addObject("ability",pokemonDescription.getAbilities());

       // modelAndView.setViewName("pokemondescription");

        return serviceResourceAssembler.toResource(pokemonDescription);
    }

    @GetMapping("/pokemons/{id}")
    //public List<Map<String, String>> list() {
    public ModelAndView page(@PathVariable int id) {

        String url = "https://pokeapi.co/api/v2/pokemon?offset=" + id + "&limit=20";

        ModelAndView modelAndView = new ModelAndView();

        List pokemons = deserilizator.Deserilizator(url);

        String idPage = ((PokemonList) pokemons.get(0)).getNext();

        int pos = idPage.indexOf("=");

         Integer next = Integer.parseInt(idPage.substring(pos + 1, pos + 3));

        Integer previous = null;
        if (next >=  20) {

            previous = next - 40;

        }

        pokemonRepository.saveAll(((PokemonList) pokemons.get(0)).getResults());

        modelAndView.addObject(((PokemonList) pokemons.get(0)).getResults());
        modelAndView.addObject("next", next);
        modelAndView.addObject("previous", previous);
        modelAndView.setViewName("pokemons");

        return modelAndView;
    }

    @GetMapping("pokemons")
    //public List<Map<String, String>> list() {
    public ModelAndView list() {

        String url = "";

        ModelAndView modelAndView = new ModelAndView();

        List pokemons = deserilizator.Deserilizator(url);

        String idPage = ((PokemonList) pokemons.get(0)).getNext();

        int pos = idPage.indexOf("=");

        String next = idPage.substring(pos + 1, pos + 3);

        pokemonRepository.saveAll(((PokemonList) pokemons.get(0)).getResults());

        modelAndView.addObject(((PokemonList) pokemons.get(0)).getResults());
        modelAndView.addObject("next", next);
        modelAndView.setViewName("pokemons");

        return modelAndView;

    }


    @GetMapping("/ability")
    public List ability() {

        List ability = abilityRepository.findAll();

        if (ability.isEmpty()) {

            ability = deserilizator.DeserilizatorAbility();

        }

       // ArrayList abilityList = ((AbilityList) ability.get(0)).getResults();
        return ability;

    }

    @GetMapping("pokemonlist")
    //public List<Map<String, String>> list() {
    public List Pokemonlist() {

        try {
            // HttpHeaders
            HttpHeaders headers = new HttpHeaders();

            //headers.setAccept(Arrays.asList(new MediaType[]{MediaType.APPLICATION_JSON}));
            // Request to return JSON format
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("User-Agent", "Google Chrome");
            HttpEntity<String> entity = new HttpEntity<String>(headers);

            // RestTemplate
            RestTemplate restTemplate = new RestTemplate();
//
//            // Send request with GET method, and Headers.
//            // ResponseEntityt<Pokemonlist> response = restTemplate.exchange("http://pokeapi.co/api/v2/pokemon"
            ResponseEntity<PokemonList> response = restTemplate.exchange("https://pokeapi.co/api/v2/pokemon/", //
                    //HttpMethod.GET, entity, new ParameterizedTypeReference<<Pokemonlist>(){});
                    HttpMethod.GET, entity, PokemonList.class);


            List result = Arrays.asList(response.getBody());

            System.out.println(result);
            logger.info(result);
            return result;

        } catch (Exception e) {

            logger.trace(e.getMessage(), e);

        }
        // HttpEntity<String>: To get result as String.
        return null;
    }

//    @GetMapping("usetoogle")
//    public String usetoogle() {
//
//        String result = domainController.PokemonServise();
//
//        return result;
//    }pageresoursesassembler

//    @GetMapping("hateoas")
//    public PokemonResource hateoas() {
//
//        Pokemon pokemon = new Pokemon("TEST", "http://TE");
//
//        return serviceResourceAssembler.toResource(pokemon);
//            return null;
//    }

//    @GetMapping("/1")
//    public List<Pokemon> MyRequest() {
//
//        Pageable pageable = PageRequest.of(1, 10);
//
//        List<Pokemon> pokemons = serviceDTO.FindAllPokemons();
//
//        Page<Pokemon> pokemon = (Page<Pokemon>) serviceDTO.findAllById(pageable, 1);
//
//       // ResponseEntity.ok(page)
//
//        return pokemon.getContent();
//
//    }

    @GetMapping("/viewpokemons")

        public List<Pokemon> MyRequest() {

        Pageable pageable = PageRequest.of(1, 100);
//
        List<Pokemon> pokemons = serviceDTO.FindAllPokemons();

        //Page<Pokemon> pokemon = (Page<Pokemon>) serviceDTO.findAllById(pageable, 1);

       // ResponseEntity.ok(page)

        return pokemons;

    }
}












