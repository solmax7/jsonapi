package com.restPokemon.Pokemon.Controller;

import com.restPokemon.Pokemon.DAO.MyCustomRepository;
import com.restPokemon.Pokemon.DAO.MyRepository;
import com.restPokemon.Pokemon.Entity.Pokemon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
public class MyRepositoryController {

    @Autowired
    MyCustomRepository myCustomRepository;

    @GetMapping("/myrequest")

    public List<Pokemon> MyRequest() {

        List<Pokemon> pokemons = myCustomRepository.getByName("charizard");
        return pokemons;

    }

}
