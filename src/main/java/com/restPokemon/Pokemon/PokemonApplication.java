package com.restPokemon.Pokemon;

import com.restPokemon.Pokemon.Configuration.InjectAnnotationBeanPostProcessor;
import com.restPokemon.Pokemon.Entity.Pokemon;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PokemonApplication {



	public static void main(String[] args) {
		SpringApplication.run(PokemonApplication.class, args);
		//new InjectAnnotationBeanPostProcessor();

	}

}
