package com.restPokemon.Pokemon.Service;

import com.restPokemon.Pokemon.DAO.PokemonRepository;
import com.restPokemon.Pokemon.Entity.Pokemon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Repository

public class ServiceDTO {

    @Autowired

    PokemonRepository pokemonRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public List<Pokemon> FindAllPokemons(){

    Pageable pageable = PageRequest.of(1, 20);

    Page<Pokemon> pokemon = pokemonRepository.findAll(pageable);//(Page<Pokemon>) serviceDTO.findAllById(pageable, 1);
    //List<Pokemon> pokemons  = pokemonRepository.findAll();
    return pokemon.getContent();
    //return pokemons;

}

    public Pokemon FindPokemonsById(Long id){

        Pageable pageable = PageRequest.of(1, 10);

        Page<Pokemon> pokemon = pokemonRepository.findAll(pageable);//(Page<Pokemon>) serviceDTO.findAllById(pageable, 1);
//
//       // ResponseEntity.ok(page)
//
//        return pokemon.getContent();

       // Pokemon pokemon  = (Pokemon) pokemonRepository.findAllById(Collections.singleton(id));

        return (Pokemon) pokemon.getContent();

    }

}
