package com.restPokemon.Pokemon.HateOAS;

import com.restPokemon.Pokemon.Controller.PokemonController;
import com.restPokemon.Pokemon.Entity.Pokemon;
import com.restPokemon.Pokemon.Entity.PokemonDescription;
import com.restPokemon.Pokemon.Entity.PokemonResource;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@Component
public class ServiceResourceAssembler extends ResourceAssemblerSupport<PokemonDescription, PokemonResource> {

    public ServiceResourceAssembler() {

        super(PokemonController.class, PokemonResource.class);

    }

    @Override
    public PokemonResource toResource(PokemonDescription pokemonDescription) {
        PokemonResource er = super.instantiateResource(pokemonDescription);
        er.setName(pokemonDescription.getName());
        er.add(getLinks(pokemonDescription));
        return er;
    }

    private Link getLinks(PokemonDescription pokemonDescription){

        Link link = linkTo(PokemonController.class).slash(pokemonDescription.getName()).withRel(pokemonDescription.getName());

        return link;
    }

}
